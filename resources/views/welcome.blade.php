<!DOCTYPE html>
<html>
  <head>
    <title>Talking with Pusher</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="content">
        <h1>Laravel 5 and Pusher is fun!</h1>
        <ul id="messages" class="list-group">
        </ul>

      </div>
    </div>
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script>
        let messageArray = [];
      //instantiate a Pusher object with our Credential's key
      var pusher = new Pusher('579c116aee7e0ae06498', {
          cluster: 'us2',
          encrypted: false
      });

      //Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('my-channel');

      //Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\HelloPusherEvent', function(message) {
        //messageArray.push('<li>'+message+'</li>')
        console.log(message.message)
        $("#messages").append("<li>"+message.message+"</li>");
        });
    </script>
  </body>
</html>