<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<input type="text" id="name">
	<button id="enviar">enviar</button>
</body>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript">
  	$('#enviar').on('click', function(){
  		let message = $('#name').val();
  		axios.get('/pusher/'+message)
		  .then(function (response) {
		    console.log('enviado');
		    console.log(response);
		  })
		  .catch(function (error) {
		    // handle error
		    console.log(error);
		  })
  	})
  </script>
</html>